import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { split } from 'apollo-link';
import { HttpLink } from 'apollo-link-http';
import { ActionCableLink } from 'graphql-ruby-client';
import ActionCable from 'actioncable';
import VueApollo from 'vue-apollo';
import { getMainDefinition } from 'apollo-utilities';
import { formatGraphQLErrors } from '../utils/errors';

const httpLink = new HttpLink({
  uri: process.env.VUE_APP_BACKEND_URL,
});

const cable = ActionCable.createConsumer(process.env.VUE_APP_WEBSOCKET_URL);
const wsLink = new ActionCableLink({ cable });

const link = split(
  // split based on operation type
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition'
      && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

const apolloClient = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  connectToDevTools: process.env.NODE_ENV === 'development',
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
  errorHandler(err, vm) {
    vm.$buefy.snackbar.open({
      duration: 5000,
      message: formatGraphQLErrors(err),
      type: 'is-danger',
      position: 'is-bottom-left',
      queue: false,
    });
  },
});

export default apolloProvider;
