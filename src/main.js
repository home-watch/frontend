import Vue from 'vue';
import Buefy from 'buefy';
import VueApollo from 'vue-apollo';
import 'buefy/dist/buefy.css';

import App from './App.vue';
import './registerServiceWorker';
import router from './router';
import store from './store';
import apolloProvider from './apollo';
import './styles/main.scss';

Vue.config.productionTip = false;

Vue.use(Buefy);
Vue.use(VueApollo);

new Vue({
  router,
  store,
  apolloProvider,
  render: (h) => h(App),
}).$mount('#app');
