// eslint-disable-next-line import/prefer-default-export
export function formatGraphQLErrors({ networkError, graphQLErrors, message }) {
  if (networkError) return networkError;

  if (graphQLErrors.length > 0) {
    console.dir(graphQLErrors);
    return graphQLErrors.reduce((msg, { message: err }) => [msg, err].join('\n'), '');
  }

  return message;
}
